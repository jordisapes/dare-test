
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = require('chai').expect

chai.use(chaiHttp)
const url = 'http://localhost:3000/api/v1'
let token
let firstUser
let firstPolicy
describe('End to end test ', () => {
    describe('Login ', () => {
        it('Login invalid user', (done) => {
            chai.request(url)
                .post('/login')
                .send({ username: 'axa', password: 'secret' })
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(401)
                    done()
                }
                )
        })
        it('Login invalid body', (done) => {
            chai.request(url)
                .post('/login')
                .send({ id: 0 })
                .end((err, res) => {
                    if (err) throw err

                    expect(res).to.have.status(400)
                    done()
                })
        })
        it('Login user success', (done) => {
            chai.request(url)
                .post('/login')
                .send({ username: 'axa', password: 's3cr3t' })
                // eslint-disable-next-line handle-callback-err
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    expect(res.body).to.have.property('token')
                    token = 'Bearer ' + res.body.token
                    done()
                })
        })
    })
    describe('Policies ', () => {
        it('GET 10 policies', (done) => {
            chai.request(url)
                .get('/policies?limit=10')
                .set('Authorization', token)
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    expect(res.body.length).to.equal(10)
                    firstPolicy = res.body[0]
                    expect(firstPolicy).to.have.property('id')
                    expect(firstPolicy).to.have.property('amountInsured')
                    expect(firstPolicy).to.have.property('email')
                    expect(firstPolicy).to.have.property('inceptionDate')
                    expect(firstPolicy).to.have.property('installmentPayment')
                    done()
                })
        })
        it('GET all policies with no authorization', (done) => {
            chai.request(url)
                .get('/policies')
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(401)
                    done()
                })
        })
        it('GET  first policy', (done) => {
            chai.request(url)
                .get('/policies/' + firstPolicy.id)
                .set('Authorization', token)
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    expect(res.body).to.have.property('id')
                    expect(res.body).to.have.property('amountInsured')
                    expect(res.body).to.have.property('email')
                    expect(res.body).to.have.property('inceptionDate')
                    expect(res.body).to.have.property('installmentPayment')
                    done()
                })
        })
    })
    describe('Clients ', () => {
        it('GET all clients', (done) => {
            chai.request(url)
                .get('/clients?limit=10')
                .set('Authorization', token)
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    expect(res.body.length).to.equal(10)
                    firstUser = res.body[0]
                    expect(firstUser).to.have.property('id')
                    expect(firstUser).to.have.property('name')
                    expect(firstUser).to.have.property('email')
                    expect(firstUser).to.have.property('role')
                    expect(firstUser).to.have.property('policies')
                    done()
                })
        })
        it('GET all policies with no authorization', (done) => {
            chai.request(url)
                .get('/policies')
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(401)
                    done()
                })
        })
        it('GET  client id', (done) => {
            chai.request(url)
                .get('/clients/' + firstUser.id)
                .set('Authorization', token)
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    expect(res.body).to.have.property('id')
                    expect(res.body).to.have.property('name')
                    expect(res.body).to.have.property('role')
                    expect(res.body).to.have.property('policies')
                    done()
                })
        })
        it('GET  client policies', (done) => {
            chai.request(url)
                .get('/clients/' + firstUser.id + '/policies')
                .set('Authorization', token)
                .end((err, res) => {
                    if (err) throw err
                    expect(res).to.have.status(200)
                    const policy = res.body[0]
                    expect(policy).to.have.property('id')
                    expect(policy).to.have.property('amountInsured')
                    expect(policy).to.have.property('inceptionDate')
                    done()
                })
        })
    })
})
