const { PoliciesManager, AuthenticationManager } = require('../../../src/managers')
const { describe, it, before, after } = require('mocha')
const { expect } = require('chai')
const { Client: ApiClient } = require('../../../src/insuranceapi')
const sinon = require('sinon')
const _ = require('underscore')
const fakePolicies = [
    {
        id: '1',
        amountInsured: '10',
        email: '1@email.com'
    },
    {
        id: '2',
        amountInsured: '10',
        email: '1@email.com'
    },
    {
        id: '3',
        amountInsured: '10',
        email: '1@email.com'
    },
    {
        id: '4',
        amountInsured: '10',
        email: '1@email.com'
    },
    {
        id: '5',
        amountInsured: '10',
        email: '1@email.com'
    },
    {
        id: '6',
        amountInsured: '10',
        email: '1@email.com'
    }
]
describe('Test  policies manager', async () => {
    describe('Unit test for policies manager', async () => {
        before(() => {
            sinon
                .stub(ApiClient.prototype, 'getPolicies')
                // eslint-disable-next-line promise/param-names
                .callsFake(() => { return new Promise((res) => res(fakePolicies)) })
            sinon
                .stub(AuthenticationManager, 'getValidAccessToken')
                // eslint-disable-next-line promise/param-names
                .callsFake(() => { return new Promise((res) => res('tokenFake')) })
        })

        after(() => {
            ApiClient.prototype.getPolicies.restore()
            AuthenticationManager.getValidAccessToken.restore()
        })
        it('Return N policies passed as argument', async () => {
            const manager = new PoliciesManager()
            const policies = await manager.getPolicies(3, 'tokenFake')
            expect(policies.length).to.be.equal(3)
        })

        it('Return all policies', async () => {
            const manager = new PoliciesManager()
            const policies = await manager.getPolicies(0, 'tokenFake')
            expect(policies.length).to.be.equal(fakePolicies.length)
        })

        it('Return expected policy', async () => {
            const manager = new PoliciesManager()
            const policy = await manager.getPolicy('4', 'tokenFake')
            expect(policy.id).to.be.equal('4')
        })
    })
    describe('Integration test for policies manager', async () => {
        it('Get 10 first policies', async () => {
            const policiesManager = new PoliciesManager()
            const { token: jwtToken } = await AuthenticationManager.login('axa', 's3cr3t')
            const policies = await policiesManager.getPolicies(10, jwtToken)
            expect(policies.length).to.be.equal(10)
        })

        it('Get policy', async () => {
            const policiesManager = new PoliciesManager()
            const { token: jwtToken } = await AuthenticationManager.login('axa', 's3cr3t')
            const policies = await policiesManager.getPolicies(10, jwtToken)
            const policeNine = policies[9]
            const policy = await policiesManager.getPolicy(policeNine.id, jwtToken)
            expect(policy.id).to.be.equal(policeNine.id)
        })
        it('Get policy that not exists', async () => {
            const policiesManager = new PoliciesManager()
            const { token: jwtToken } = await AuthenticationManager.login('axa', 's3cr3t')
            const policy = await policiesManager.getPolicy('Inexistente', jwtToken)
            expect(_.isEmpty(policy)).to.be.equal(true)
        })
    })
})
