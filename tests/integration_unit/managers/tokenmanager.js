const tokenManager = require('../../../src/managers/TokenManager')
const { describe, it } = require('mocha')
const { expect } = require('chai')
describe('Unit test token manager', async () => {
    describe('Tests about how tokenmanager operates', async () => {
        it('Generate token properly', async () => {
            const { token: jwtToken } = tokenManager.generateAccessToken('myToken', 'myUser', 'mySecret')

            expect(tokenManager.isValidToken(jwtToken)).to.be.equal(true)
            const { user, secret } = tokenManager.getDecodedToken(jwtToken)
            expect(user).to.be.equal('myUser')
            expect(secret).to.be.equal('mySecret')
            expect(tokenManager.getAccessToken(jwtToken)).to.be.equal('myToken')
        })

        it('Send invalid token and get token is invalid ', async () => {
            const { token } = tokenManager.generateAccessToken('renewToken2', 'myUser', 'mySecret')
            expect(tokenManager.isValidToken('tokenraro')).to.be.equal(false)
            expect(tokenManager.isValidToken(token)).to.be.equal(true)
        })
    })
})
