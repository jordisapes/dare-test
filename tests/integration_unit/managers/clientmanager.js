const { PoliciesManager, AuthenticationManager, ClientsManager } = require('../../../src/managers')
const { describe, it, before, after } = require('mocha')
const { expect } = require('chai')
const { Client: ApiClient } = require('../../../src/insuranceapi')
const sinon = require('sinon')
const _ = require('underscore')
const fakePolicies = [
    {
        id: '1',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '1'
    },
    {
        id: '2',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '1'
    },
    {
        id: '3',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '2'
    },
    {
        id: '4',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '2'
    },
    {
        id: '5',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '3'
    },
    {
        id: '6',
        amountInsured: '10',
        email: '1@email.com',
        clientId: '3'
    }
]
const fakeUsers = [
    {
        id: '1',
        name: 'a',
        role: 'user'
    },
    {
        id: '2',
        name: 'a',
        role: 'admin'
    },
    {
        id: '3',
        name: 'c',
        role: 'user'
    }
]
describe('Test  clients manager', async () => {
    describe('Unit test for clients manager', async () => {
        before(() => {
            sinon
                .stub(ApiClient.prototype, 'getClients')
                // eslint-disable-next-line promise/param-names
                .callsFake(() => { return new Promise((res) => res(fakeUsers)) })
            sinon
                .stub(AuthenticationManager, 'getValidAccessToken')
                // eslint-disable-next-line promise/param-names
                .callsFake(() => { return new Promise((res) => res('tokenFake')) })
            sinon
                .stub(PoliciesManager.prototype, 'getPolicies')
                // eslint-disable-next-line promise/param-names
                .callsFake(() => { return new Promise((res) => res(fakePolicies)) })
        })

        after(() => {
            ApiClient.prototype.getClients.restore()
            AuthenticationManager.getValidAccessToken.restore()
            PoliciesManager.prototype.getPolicies.restore()
        })
        it('Return 1 client with his policies (user and admin)', async () => {
            const manager = new ClientsManager()
            const client = await manager.getClient('1', 'myToken')
            expect(client.id).to.be.equal('1')
            expect(client).to.have.property('policies')
            expect(client.policies.length).to.be.equal(2)
        })
        it('Ask for a client with no name', async () => {
            const manager = new ClientsManager()
            const client = await manager.getClient('1', 'myToken', 'no name')
            expect(_.isEmpty(client)).to.be.equal(true)
        })
        it('Ask for a client with  name', async () => {
            const manager = new ClientsManager()
            const client = await manager.getClient('1', 'myToken', 'a')
            expect(client.id).to.be.equal('1')
            expect(client.name).to.be.equal('a')
            expect(client).to.have.property('policies')
            expect(client.policies.length).to.be.equal(2)
        })

        it('Return policies from a client', async () => {
            const manager = new ClientsManager()
            const policies = await manager.getClientPolicies('1', 'myToken')
            expect(policies.length).to.be.equal(2)
        })

        it('Get all clients', async () => {
            const manager = new ClientsManager()
            const clients = await manager.getClients(0, 'tokenFake')
            expect(clients.length).to.be.equal(fakeUsers.length)
            for (const client of clients) {
                expect(client).to.have.property('policies')
                expect(client.policies.length).to.be.equal(2)
            }
        })

        it('Get all clients with name', async () => {
            const manager = new ClientsManager()
            const clients = await manager.getClients(0, 'tokenFake', 'a')
            expect(clients.length).to.be.equal(2)
            for (const client of clients) {
                expect(client).to.have.property('policies')
                expect(client.policies.length).to.be.equal(2)
            }
        })

        it('Get all clients with no name', async () => {
            const manager = new ClientsManager()
            const clients = await manager.getClients(0, 'tokenFake', 'no name')
            expect(clients.length).to.be.equal(0)
        })
    })
    describe('Integration test for client manager', async () => {
        it('Get all clients', async () => {
            const manager = new ClientsManager()
            const { token: authToken } = await AuthenticationManager.login('axa', 's3cr3t')
            const clients = await manager.getClients(10, authToken)
            for (const client of clients) {
                expect(client).to.have.property('policies')
            }
        })
        it('Get client id policy', async () => {
            const manager = new ClientsManager()
            const { token: authToken } = await AuthenticationManager.login('axa', 's3cr3t')
            const clients = await manager.getClients(10, authToken)
            const client = clients[4]
            const respClient = await manager.getClient(client.id, authToken)
            expect(respClient.id).to.be.equal(client.id)
        })
    })
})
