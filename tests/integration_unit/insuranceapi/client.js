const Client = require('../../../src/insuranceapi/client')
const { describe, it } = require('mocha')
const { expect } = require('chai')
const url = 'https://dare-nodejs-assessment.herokuapp.com/api'
describe('Integration insurance api test', async () => {
    describe('Login', async () => {
        it('Login success', async () => {
            const client = new Client(url)
            const response = await client.login({ clientId: 'axa', clientSecret: 's3cr3t' })
            expect(response.type).to.be.equal('Bearer')
            expect(response).to.have.property('token')
        })

        it('Login failed', async () => {
            try {
                const client = new Client(url)
                await client.login({ clientId: 'axa', clientSecret: 'secret' })
            } catch (e) {
                expect(e.details.statusCode).to.be.equal(401)
                expect(e.status).to.be.equal(401)
            }
        })
    })
    describe('Get Clients', async () => {
        it('Get clients', async () => {
            const client = new Client(url)
            const responseLogin = await client.login({ clientId: 'axa', clientSecret: 's3cr3t' })
            const clients = await client.getClients(responseLogin.token)
            expect(clients).to.be.a('Array')
            for (const c of clients) {
                expect(c).to.have.property('id')
                expect(c).to.have.property('name')
                expect(c).to.have.property('email')
                expect(c).to.have.property('role')
            }
        })
        it('Get clients without login', async () => {
            try {
                const client = new Client(url)
                await client.getClients('TEST')
            } catch (e) {
                expect(e.details.statusCode).to.be.equal(401)
                expect(e.status).to.be.equal(401)
            }
        })
    })
    describe('Get policies', async () => {
        it('Get policies', async () => {
            const client = new Client(url)
            const responseLogin = await client.login({ clientId: 'axa', clientSecret: 's3cr3t' })
            const policies = await client.getPolicies(responseLogin.token)
            expect(policies).to.be.a('Array')
            for (const p of policies) {
                expect(p).to.have.property('id')
                expect(p).to.have.property('amountInsured')
                expect(p).to.have.property('inceptionDate')
                expect(p).to.have.property('installmentPayment')
                expect(p).to.have.property('clientId')
            }
        })
        it('Get policies without login', async () => {
            try {
                const client = new Client(url)
                await client.getPolicies('TEST')
            } catch (e) {
                expect(e.details.statusCode).to.be.equal(401)
                expect(e.status).to.be.equal(401)
            }
        })
    })
})
