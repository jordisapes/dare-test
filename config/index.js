let exportModule
switch (process.env.environment) {
    case 'production':
        exportModule = require('./production/config.json')
        break
    case 'pre':
        exportModule = require('./preproduction/config.json')
        break
    default:
        exportModule = require('./development/config.json')
        break
}
module.exports = { config: exportModule }
