# Dare-test

## Run application

    $ cd <base_dir> 
    $ npm install
    $ npm start

## Run tests

There are 3 types of tests. Unit test, integration tests and end to end test.
End to end tests needs to have the application running in background

### Unit and integration tests

    $ cd <base_dir> 
    $ npm install
    $ npm run test
    
### End to end Tests

IMPORTANT: Server must be listening on port 3000

    $ cd <base_dir> 
    $ npm install
    $ npm run start
    $ npm run e2e_test

### Run all tests

IMPORTANT: Server must be listening on port 3000

    $ cd <base_dir> 
    $ npm install
    $ npm run start
    $ npm run e2e_test

## Project structure

In this section will be explained the folder structure and project layers

- /config : Folder that contains config options. Ex: Insurance api url, token ttl, ...  
- /src : Container folder with source code
- /src/controllers : It has all the controllers from endpoints of the api that validates the input parameters
- /src/insuranceapi : Folder with the client implementation to connect at insurance api
- /src/managers : Folder that contains different managers. It is the business layer
- /src/models : Model layer. This folder contains the specific models of the API
- /src/routes : Folder that publish all the enpoints and link to his controllers
- /test: Folder containing all the api tests


## Some comments about test

### Some decisions taken
- I return all policies parameters that has insurance api. It is easy to only send swagger parameters if a new model 
is created, but I prefer return the maximum arguments possible

- In swagger documentation of the api to build it says the following ``Can be accessed by client with role user (it will
 retrieve its own client details as only element of the list) and admin (it will retrieve all the clients 
 list)``. Where I can get the role about the user logged? In login endpoint only returns the token, there is 
 no information about the role
 
 - Token renovation: Api insurance doesn't provide TTL token time. In case application
  receive a valid token and when access to the API insurance and response with a 401 error, it will try to 
  get a new access token.   

### Some swagger faults

- In endpoints that has :id swagger doesn't pass the id passed as a param. It always pass :id. 

