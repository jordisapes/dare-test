const { AuthenticationManager } = require('../managers')
const { AppError } = require('../models')
module.exports = class AuthCtrl {
    static async login(req, res, next) {
        try {
            const { username, password } = req.body
            if (!username || !password) {
                return next(new AppError({
                    status: 400, message: 'Invalid body received',
                    details: 'username: ' + username + 'password: ' + password,
                    error: 'Bad Request'
                }))
            }
            const response = await AuthenticationManager.login(username, password)
            res.status(200).send(response)
        } catch (e) {
            return next(e)
        }
    }
}
