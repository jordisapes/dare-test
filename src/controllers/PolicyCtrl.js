const { PoliciesManager, AuthenticationManager } = require('../managers')
const policiesManager = new PoliciesManager()
module.exports = class PolicyCtrl {
    static async getPolicies(req, res, next) {
        try {
            req.query = req.query || {}
            const response = await policiesManager.getPolicies(req.query.limit || 0, req.headers.authorization, req.query.name)
            res.status(200).send(response)
        } catch (e) {
            await PolicyCtrl.handleError(e, next, res, PolicyCtrl.getPolicies, req.query.limit || 0, req.headers.authorization, req.query.name)
        }
    }

    static async getPolicy(req, res, next) {
        try {
            req.query = req.query || {}
            const response = await policiesManager.getPolicy(req.params.id, req.headers.authorization)
            res.status(200).send(response)
        } catch (e) {
            await PolicyCtrl.handleError(e, next, res, PolicyCtrl.getPolicy, req.params.id || 0, req.headers.authorization, null)
        }
    }

    static async handleError(error = {}, next, res, method, param1, token, param3) {
        if (error.status === 401) {
            const newToken = await AuthenticationManager.renewToken(token)
            const response = await method(param1, newToken, param3)
            res.status(200).send(response)
        } else {
            return next(error)
        }
    }
}
