const AuthCtrl = require('./AuthCtrl')
const ClientCtrl = require('./ClientCtrl')
const PolicyCtrl = require('./PolicyCtrl')

module.exports = { AuthCtrl, ClientCtrl, PolicyCtrl }
