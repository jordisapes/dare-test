const { ClientsManager, AuthenticationManager } = require('../managers')
const clientsManager = new ClientsManager()
module.exports = class ClientCtrl {
    static async getClients(req, res, next) {
        try {
            req.query = req.query || {}
            const response = await clientsManager.getClients(req.query.limit || 0, req.headers.authorization, req.query.name)
            res.status(200).send(response)
        } catch (e) {
            await ClientCtrl.handleError(e, next, res, ClientCtrl.getClients,
                req.params.limit || 0, req.headers.authorization, req.query)
        }
    }

    static async getClient(req, res, next) {
        try {
            req.query = req.query || {}
            const response = await clientsManager.getClient(req.params.id, req.headers.authorization, req.query.name)
            res.status(200).send(response)
        } catch (e) {
            await ClientCtrl.handleError(e, next, res, ClientCtrl.getClient,
                req.params.id, req.headers.authorization, req.query)
        }
    }

    static async getClientPolicies(req, res, next) {
        try {
            req.query = req.query || {}
            const response = await clientsManager.getClientPolicies(req.params.id, req.headers.authorization, req.query.name)
            res.status(200).send(response)
        } catch (e) {
            await ClientCtrl.handleError(e, next, res, ClientCtrl.getClientPolicies,
                req.params.id, req.headers.authorization, req.query)
        }
    }

    static async handleError(error = {}, next, res, method, param1, token, param3) {
        if (error.status === 401) {
            const newToken = await AuthenticationManager.renewToken(token)
            const response = await method(param1, newToken, param3)
            res.status(200).send(response)
        } else {
            return next(error)
        }
    }
}
