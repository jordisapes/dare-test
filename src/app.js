// const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const AppError = require('./models/AppError')
const authRouter = require('./routes/auth')
const clientsRouter = require('./routes/clients')
const policiesRouter = require('./routes/policies')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/api/v1', [authRouter, clientsRouter, policiesRouter])

// app.use('/', outer)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(new AppError({
        status: 404, message: 'Route not found',
        error: 'Not found'
    }))
})

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    if (err instanceof AppError) {
        res.status(err.status).send({ code: err.status, message: err.toString() })
    } else {
        const appError = new AppError(
            {
                status: err.status || 500,
                message: err.message || err.stack,
                details: err.toString(),
                error: 'Uncontrolled error'
            })

        res.status(appError.status || 500).send({ code: appError.status, message: appError.toString() })
    }
    // res.locals.message = err.message
    // res.locals.error = req.app.get('env') === 'development' ? err : {}
    //
    // // render the error page
    // res.status(err.status || 500)
    // res.render('error')
})

module.exports = app
