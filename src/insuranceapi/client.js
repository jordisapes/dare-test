const { Client: RestClient } = require('node-rest-client')
const debug = require('debug')('daretest:apiclient')
const _ = require('underscore')
const { AppError } = require('../models/')
const ENDPOINTS = {
    getClients: {
        name: 'getClients', endpoint: '/clients'
    },
    getPolicies: {
        name: 'getPolicies', endpoint: '/policies'
    },
    login: {
        name: 'login', endpoint: '/login'
    }
}
const HTTP_METHODS = {
    get: 'GET', post: 'POST'
}

class Client {
    constructor(url) {
        this.url = url
        this.restClient = new RestClient()
        this.restClient.registerMethod(ENDPOINTS.getClients.name, this.url + ENDPOINTS.getClients.endpoint, HTTP_METHODS.get)
        this.restClient.registerMethod(ENDPOINTS.getPolicies.name, this.url + ENDPOINTS.getPolicies.endpoint, HTTP_METHODS.get)
        this.restClient.registerMethod(ENDPOINTS.login.name, this.url + ENDPOINTS.login.endpoint, HTTP_METHODS.post)
    }

    /**
     * @returns [Client] Clients
     */
    async getClients(token) {
        return await this._sendRequest(ENDPOINTS.getClients.name, this._getHeaders(token))
    }

    _getHeaders(token) {
        return { headers: { authorization: 'Bearer ' + token } }
    }

    /**
     * @returns [Policy] policies
     */
    async getPolicies(token) {
        return await this._sendRequest(ENDPOINTS.getPolicies.name, this._getHeaders(token))
    }

    /**
     *
     * @param {string} clientId
     * @param {string} clientSecret
     * @return {string}
     */
    async login({ clientId = '', clientSecret = '' }) {
        return await this._sendRequest(ENDPOINTS.login.name, {
            data: { client_id: clientId, client_secret: clientSecret },
            headers: { 'Content-Type': 'application/json' }
        })
    }

    _isSuccessResponse(apiResponse) {
        return !_.has(apiResponse, 'statusCode')
    }

    _handleApiError(responseError) {
        const formattedMessage = {}
        switch (responseError.statusCode) {
            case (401):
                formattedMessage.status = 401
                formattedMessage.error = 'unauthorized'
                formattedMessage.message = responseError.message
                formattedMessage.details = responseError
                break
            default :
                formattedMessage.status = responseError.statusCode
                formattedMessage.error = 'Non controlled error'
                formattedMessage.message = responseError.message
                formattedMessage.details = responseError
                break
        }
        debug(formattedMessage)
        return new AppError(formattedMessage)
    }

    async _sendRequest(clientMethod, args) {
        // eslint-disable-next-line promise/param-names
        return new Promise((res, rej) => {
            this.restClient.methods[clientMethod](args, (data, response) => {
                if (this._isSuccessResponse(data)) {
                    res(data)
                } else {
                    rej(this._handleApiError(data))
                }
            })
        })
    }
}

module.exports = Client
