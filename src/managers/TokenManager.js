const moment = require('moment')
const jwt = require('jwt-simple')
const { config } = require('../../config')

class TokenManager {
    /**
     * Class that manages tokens
     * @param ttl Time to live in minutues
     * @param secret
     */
    constructor(ttl, secret) {
        // this.savedTokens = {}
        this.ttl = ttl
        this.secret = secret
    }

    /**
     * Check token is not expired
     * @param token
     * @return {boolean}
     */
    isTokenExpired(token) {
        const jwtToken = this.getDecodedToken(token)
        return moment().isAfter(moment(jwtToken.expires))
    }

    /**
     * Returns decoded token
     * @param jwtToken
     * @return {{user: string, secret: string, expires: string, token: string}}
     */
    getDecodedToken(jwtToken) {
        return jwt.decode(jwtToken, this.secret)
    }

    /**
     *
     * @param authToken
     * @param user
     * @param secret
     * @return {{expires_in: Number, type: string, token: string}}
     */
    generateAccessToken(authToken, user, secret) {
        const momentExpires = moment().add(this.ttl, 'm')
        const payload = {
            token: authToken, user: user, secret: secret,
            expires: momentExpires
        }
        const jwtToken = jwt.encode(payload, this.secret)
        return { token: jwtToken, expires_in: this.ttl, type: 'Bearer' }
    }

    /**
     * Check token is already validated
     * @param token
     * @return {*|boolean}
     */
    isValidToken(token) {
        try {
            this.getDecodedToken(token)
            return true
        } catch (e) {
            return false
        }
    }

    /**
     * Returns access token
     * @param token
     * @return {string}
     */
    getAccessToken(token) {
        const { token: accessToken } = this.getDecodedToken(token)
        return accessToken
    }
}

module.exports = new TokenManager(config.ttl, config.secret)
