const { config } = require('../../config')
const { Client: ApiClient } = require('../insuranceapi')
const _ = require('underscore')
const authenticationManager = require('./AuthenticationManager')
class PoliciesManager {
    constructor() {
        this.apiClient = new ApiClient(config.apiUrl)
    }

    /**
     * Get policies
     * @param limit
     * @param token
     * @return {Promise<[]|*>}
     */
    async getPolicies(limit = 10, token) {
        const policies = await this.apiClient.getPolicies(await authenticationManager.getValidAccessToken(token))
        if (limit === 0) {
            return policies
        }
        return _.first(policies, limit)
    }

    /**
     * Get policy
     * @param id
     * @param accessToken
     * @return {Promise<{}>}
     */
    async getPolicy(id, accessToken) {
        const policies = await this.getPolicies(0, accessToken)
        const policy = _.findWhere(policies, { id: id })
        return policy || {}
    }
}

module.exports = PoliciesManager
