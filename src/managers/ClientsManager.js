const { config } = require('../../config')
const { Client: ApiClient } = require('../insuranceapi')
const _ = require('underscore')
const PoliciesManager = require('./PoliciesManager')
const authenticationManager = require('./AuthenticationManager')
// const CLIENT_ROLES = {
//     user: 'user', admin: 'admin'
// }
class ClientsManager {
    constructor() {
        this.apiClient = new ApiClient(config.apiUrl)
        this.policiesMngr = new PoliciesManager()
    }

    /**
     * Get clients asking to api insurance
     * @param limit
     * @param token
     * @param name
     * @return {Promise<*>}
     */
    async getClients(limit = 10, token, name) {
        const requests = [
            this._getFilteredClients(token, name),
            this.policiesMngr.getPolicies(0, token)
        ]
        const responseRequests = await Promise.all(requests)
        if (limit === 0) {
            return this._fillClients(responseRequests[0], responseRequests[1])
        }
        const clients = _.first(responseRequests[0], limit)
        return this._fillClients(clients, responseRequests[1])
    }

    async _getFilteredClients(token, name) {
        const clients = await this.apiClient.getClients(
            await authenticationManager.getValidAccessToken(token))
        if (name) {
            return clients.filter(client => client.name === name)
        } else {
            return clients
        }
    }

    /**
     * Get client by id
     * @param clientId
     * @param token
     * @param name
     * @return {Promise<{}|*>}
     */
    async getClient(clientId, token, name) {
        const requests = [
            this._getFilteredClients(token, name),
            this.policiesMngr.getPolicies(0, token)
        ]
        const responseRequests = await Promise.all(requests)
        const client = _.findWhere(responseRequests[0], { id: clientId })
        if (!client) {
            return {}
        }
        return this._fillClient(client, responseRequests[1])
    }

    /**
     * Get client policies
     * @param clientId
     * @param name
     * @param token
     * @return {Promise<*[]>}
     */
    async getClientPolicies(clientId, name, token) {
        const client = await this.getClient(clientId, name, token)
        return client.policies
    }

    _fillClient(client, policies = []) {
        client.policies = policies.filter(policy => policy.clientId === client.id)
        return client
    }

    _fillClients(clients, policies) {
        return clients.map(c => {
            return this._fillClient(c, policies)
        })
    }
}

module.exports = ClientsManager
