const tokenManager = require('./TokenManager')
const { config } = require('../../config')
const { Client: ApiClient } = require('../insuranceapi')
const { AppError } = require('../models/')
class AuthenticationManager {
    constructor() {
        this.client = new ApiClient(config.apiUrl)
    }

    /**
     * Return access token
     * @param authToken
     * @return {Promise<string>}
     */
    async getValidAccessToken(authToken) {
        if (tokenManager.isValidToken(authToken) && !tokenManager.isTokenExpired(authToken)) {
            return tokenManager.getAccessToken(authToken)
        } else {
            throw new AppError({
                status: 401, error: 'Unauthorized', message: 'Invalid token received',
                details: 'Be sure token is not expired'
            })
        }
    }

    /**
     * Get user and secret from token and make login to client to renew token
     * @param oldToken
     * @return {Promise<string>}
     */
    async renewToken(oldToken) {
        if (tokenManager.isValidToken()) {
            const { user, secret } = tokenManager.getDecodedToken(oldToken)
            const { token: accessToken } = await this.login(user, secret)
            const newJwtToken = tokenManager.generateAccessToken(accessToken, user, secret)
            return tokenManager.getAccessToken(newJwtToken)
        }
    }

    /**
     *
     * @param user
     * @param secret
     * @return {Promise<{expires: moment.Moment, type: string, token: string}>}
     */
    async login(user, secret) {
        const { token: authToken } = await this.client.login({ clientId: user, clientSecret: secret })
        return tokenManager.generateAccessToken(authToken, user, secret)
    }
}

module.exports = new AuthenticationManager()
