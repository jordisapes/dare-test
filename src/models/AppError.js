
class AppError {
    constructor({ status, message, details, error }) {
        this.status = status
        this.message = message
        this.details = details || {}
        this.error = error
    }

    toString() {
        return '(' + this.status + ') Error: ' + this.error +
            ' | Message error: ' + this.message +
            ' | Details: ' + JSON.stringify(this.details)
    }
}

module.exports = AppError
