const express = require('express')
const router = express.Router()
const { ClientCtrl } = require('../controllers')
/* GET users listing. */
router.get('/clients', ClientCtrl.getClients)

router.get('/clients/:id', ClientCtrl.getClient)

router.get('/clients/:id/policies', ClientCtrl.getClientPolicies)

module.exports = router
