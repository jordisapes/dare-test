const express = require('express')
const router = express.Router()
const { AuthCtrl } = require('../controllers')
const { TokenManager } = require('../managers')
const { AppError } = require('../models')
router.post('/login', AuthCtrl.login)
router.use('/', (req, res, next) => {
    const tokenBearer = req.headers.authorization
    if (!tokenBearer) {
        return next(new AppError({
            status: 401, message: 'Token not received in header', error: 'Unauthorized'
        }))
    }
    const token = tokenBearer.split(' ')[1]
    if (!TokenManager.isValidToken(token) || TokenManager.isTokenExpired(token)) {
        return next(new AppError({
            status: 401, message: 'Invalid or expired token', error: 'Unauthorized'
        }))
    }
    req.headers.authorization = token
    return next()
})

module.exports = router
