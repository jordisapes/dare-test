const express = require('express')
const router = express.Router()
const { PolicyCtrl } = require('../controllers')

/* GET users listing. */
router.get('/policies', PolicyCtrl.getPolicies)

router.get('/policies/:id', PolicyCtrl.getPolicy)

module.exports = router
